document.getElementById('callApiBtn').addEventListener('click', function() {
    const apiSelect = document.getElementById('apiSelect');
    const selectedApi = apiSelect.options[apiSelect.selectedIndex].value;
    if (selectedApi) {
        fetch(selectedApi)
            .then(response => response.json())
            .then(data => {
                document.getElementById('responsePre').textContent = JSON.stringify(data, null, 2);
            })
            .catch(error => {
                document.getElementById('responsePre').textContent = 'An error occurred: ' + error;
            });
    } else {
        alert('Please select an API');
    }
});
